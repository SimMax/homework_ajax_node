'use strict'
/*--------axios-currency--------*/
let curDate = document.getElementById('currency_date');
curDate.value = `${todayDate()}`;
curDate.max = `${todayDate()}`;

function todayDate(){
    let date = new Date();
    let todayMonth = (date.getMonth()+1 < 10) ? `0${date.getMonth()+1}` : `${date.getMonth()+1}`;
    return `${date.getFullYear()}-${todayMonth}-${date.getDate()}`;
}

getTodayCur();

async function getTodayCur() {
    let todayCur = await axios.get('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5').catch((e) => alert(`${e}`));
    let currency = document.getElementById('currency');

    let list = currency.getElementsByTagName('p');
    for(let i = list.length - 1; i >= 0; i--) {
       list[i].remove();
    }
    
    for (let elem in todayCur.data) {
        if(todayCur.data[elem].ccy == 'USD' || todayCur.data[elem].ccy == 'EUR' || todayCur.data[elem].ccy == 'RUR'){
            let curItem = document.createElement('p');
            switch(todayCur.data[elem].ccy){
                case 'USD':{ 
                    curItem.innerHTML = `<i class="fas fa-dollar-sign"></i>: buy-${parseFloat(todayCur.data[elem].buy)}, sale-${parseFloat(todayCur.data[elem].sale)}`;
                    break;
                }
                case 'EUR':{
                    curItem.innerHTML = `<i class="fas fa-euro-sign"></i>: buy-${parseFloat(todayCur.data[elem].buy)}, sale-${parseFloat(todayCur.data[elem].sale)}`;
                    break;
                }
                case 'RUR':{
                    curItem.innerHTML = `<i class="fas fa-ruble-sign"></i>: buy-${parseFloat(todayCur.data[elem].buy)}, sale-${parseFloat(todayCur.data[elem].sale)}`;
                    break;
                }
            }
            currency.appendChild(curItem);
        }
    }
}
  
function getPastCur() {
    let date = curDate.value.split('-').join('');
    let currency = document.getElementById('currency');

    let list = currency.getElementsByTagName('p');
    for(let i = list.length - 1; i >= 0; i--) {
       list[i].remove();
    }

    Promise.all([
        axios.get(`https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&date=${date}&json`),
        axios.get(`https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=EUR&date=${date}&json`),
        axios.get(`https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=RUB&date=${date}&json`)
        ])
        .then(result => {
            result.forEach( elem => {
                let curItem = document.createElement('p');
                switch(elem.data[0].cc){
                    case 'USD':{ 
                        curItem.innerHTML = `<i class="fas fa-dollar-sign"></i>: rate-${elem.data[0].rate.toFixed(3)}`;
                        break;
                    }
                    case 'EUR':{
                        curItem.innerHTML = `<i class="fas fa-euro-sign"></i>: rate-${elem.data[0].rate.toFixed(3)}`;
                        break;
                    }
                    case 'RUB':{
                        curItem.innerHTML = `<i class="fas fa-ruble-sign"></i>: rate-${elem.data[0].rate.toFixed(3)}`;
                        break;
                    }
                }
                currency.appendChild(curItem);
            });
        }).catch((e) => {alert(`${e}`)});
}

document.getElementById('cur_btn').addEventListener('click', function(){
    (curDate.value === todayDate()) ? getTodayCur() : getPastCur();
});

/*--------axios-weather--------*/
let temp = document.createElement('p');
let pressure = document.createElement('p');
let humidity = document.createElement('p');
let wind = document.createElement('p');
let clouds = document.createElement('p');

weatherInit('Kyiv');

async function weatherInit(city){
    let obj = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=dc2e97b74d8a60f99ea2af5cbb467920`).catch((e) => alert(`${e}`));
                           
    let weather = document.getElementById('weather');

    temp.innerHTML = `Температура: ${parseInt(obj.data.main.temp - 273.15)} <sup>o</sup>C`;
    weather.appendChild(temp);

    pressure.innerText = `Давление: ${parseInt(obj.data.main.pressure * 0.75006375541921)} мм рт. ст.`;
    weather.appendChild(pressure);

    humidity.innerText = `Влажность: ${obj.data.main.humidity} %`;
    weather.appendChild(humidity);

    wind.innerText = `Ветер: ${obj.data.wind.speed} м/с`;
    weather.appendChild(wind);

    clouds.innerText = `Облачнось: ${obj.data.clouds.all} %`;
    weather.appendChild(clouds);
                            
};

document.getElementById('weather_btn').addEventListener('click', function(){
    let list = document.getElementById('cities').options;
    for(let i = 0; i < list.length; i++){
        if(list[i].selected){
            weatherInit(list[i].value);
        }
    }
});



