'use strict'
/*--------jq-currency--------*/
let curDate = $('#currency_date');
curDate.attr({'value': `${todayDate()}`, 'max': `${todayDate()}`});

function todayDate(){
    let date = new Date();
    let todayMonth = (date.getMonth()+1 < 10) ? `0${date.getMonth()+1}` : `${date.getMonth()+1}`;
    return `${date.getFullYear()}-${todayMonth}-${date.getDate()}`;
}

getTodayCur();

function getTodayCur() {
    let list = $('#currency > p');
    for(let i = list.length - 1; i >= 0; i--) {
        list[i].remove();
    }

    $.ajax({
        url: 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5',
    })
    .done( (obj) => {
        obj.forEach( elem => {
            let curItem = $('<p></p>');
            if(elem.ccy === 'USD' || elem.ccy === 'EUR' || elem.ccy === 'RUR'){
                switch(elem.ccy){
                    case 'USD':{
                        curItem.html(`<i class="fas fa-dollar-sign"></i>: buy-${parseFloat(elem.buy)}, sale-${parseFloat(elem.sale)}`);
                        break;
                    }
                    case 'EUR':{
                        curItem.html(`<i class="fas fa-euro-sign"></i>: buy-${parseFloat(elem.buy)}, sale-${parseFloat(elem.sale)}`);
                        break;
                    }
                    case 'RUR':{
                        curItem.html(`<i class="fas fa-ruble-sign"></i>: buy-${parseFloat(elem.buy)}, sale-${parseFloat(elem.sale)}`);
                        break;
                    }
                }
                $('#currency').append(curItem);
            }
        });
    })
    .fail((e) => {
        alert(e);
    });
}

function getPastCur() {
    let date = curDate.val().split('-').join('');

    let list = $('#currency > p');
    for(let i = list.length - 1; i >= 0; i--) {
        list[i].remove();
    }

    getData(['USD','EUR','RUB'], date);

    function getData(currents, date){
        currents.forEach( elem => {
            let curItem = $('<p></p>');
            $.ajax({
                url: `https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=${elem}&date=${date}&json`,
            })
            .done( (obj) => {
                switch(elem){
                    case 'USD':{
                        curItem.html(`<i class="fas fa-dollar-sign"></i>: rate-${obj[0].rate.toFixed(3)}`);
                        break;
                    }
                    case 'EUR':{
                        curItem.html(`<i class="fas fa-euro-sign"></i>: rate-${obj[0].rate.toFixed(3)}`);
                        break;
                    }
                    case 'RUB':{
                        curItem.html(`<i class="fas fa-ruble-sign"></i>: rate-${obj[0].rate.toFixed(3)}`);
                        break;
                    }
                }
                $('#currency').append(curItem);
            })
            .fail((e) => {
                alert(e);
            });
        });
    }
}

$('#cur_btn').on('click', function(){
    (curDate.val() === todayDate()) ? getTodayCur() : getPastCur();
});

/*--------jq-weather--------*/
let temp = $('<p></p>'), 
pressure = $('<p></p>'), 
humidity = $('<p></p>'), 
wind = $('<p></p>'), 
clouds = $('<p></p>');

dataInitialization('Kyiv');

function dataInitialization(city){
    $.ajax({
        url: `https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=dc2e97b74d8a60f99ea2af5cbb467920`,
    })
    .done( (obj) => {
        let weather = $('#weather');

        temp.html(`Температура: ${parseInt(obj.main.temp - 273.15)} <sup>o</sup>C`);
        weather.append(temp);

        pressure.html(`Давление: ${parseInt(obj.main.pressure * 0.75006375541921)} мм рт. ст.`);
        weather.append(pressure);

        humidity.html(`Влажность: ${obj.main.humidity} %`);
        weather.append(humidity);

        wind.html(`Ветер: ${obj.wind.speed} м/с`);
        weather.append(wind);

        clouds.html(`Облачнось: ${obj.clouds.all} %`);
        weather.append(clouds);
    })
    .fail((e) => {
        alert(e);
    });
};

$('#weather_btn').on('click', function(){
    let list = $('#cities > option');
    for(let i = 0; i < list.length; i++){
        if(list[i].selected){
            dataInitialization(list[i].value);
        }
    }
});