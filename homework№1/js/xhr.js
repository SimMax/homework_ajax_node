'use strict'
/*--------XMLHttpRequest-currency--------*/
let curDate = document.getElementById('currency_date');
curDate.value = `${todayDate()}`;
curDate.max = `${todayDate()}`;

function todayDate(){
    let date = new Date();
    let todayMonth = (date.getMonth()+1 < 10) ? `0${date.getMonth()+1}` : `${date.getMonth()+1}`;
    return `${date.getFullYear()}-${todayMonth}-${date.getDate()}`;
}

getTodayCur();

function getTodayCur() {
    let list = currency.getElementsByTagName('p');
    for(let i = list.length - 1; i >= 0; i--) {
       list[i].remove();
    }

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5', false);
    xhr.send();
    if(xhr.status != 200){
        alert(xhr.statusText);
    } else {
        let obj = JSON.parse(xhr.responseText);
        let cur = document.getElementById('currency');
        obj.forEach( elem => {
            let p = document.createElement('p');
            if(elem.ccy === 'USD' || elem.ccy === 'EUR' || elem.ccy === 'RUR'){
                switch(elem.ccy){
                    case 'USD':{
                        p.innerHTML = `<i class="fas fa-dollar-sign"></i>: buy-${parseFloat(elem.buy)}, sale-${parseFloat(elem.sale)}`;
                        break;
                    }
                    case 'EUR':{
                        p.innerHTML = `<i class="fas fa-euro-sign"></i>: buy-${parseFloat(elem.buy)}, sale-${parseFloat(elem.sale)}`;
                        break;
                    }
                    case 'RUR':{
                        p.innerHTML = `<i class="fas fa-ruble-sign"></i>: buy-${parseFloat(elem.buy)}, sale-${parseFloat(elem.sale)}`;
                        break;
                    }
                }
                cur.appendChild(p);
            }
        });
    }
}

function getPastCur() {
    let date = curDate.value.split('-').join('');
    let currency = document.getElementById('currency');

    let list = currency.getElementsByTagName('p');
    for(let i = list.length - 1; i >= 0; i--) {
       list[i].remove();
    }

    getData(['USD','EUR','RUB'], date);

    function getData(currents, date){
        currents.forEach( elem => {
            let curItem = document.createElement('p');

            let xhr = new XMLHttpRequest();
            xhr.open('GET', `https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=${elem}&date=${date}&json`, false);
            xhr.send();
            if(xhr.status != 200){
                alert(xhr.statusText);
            } else {
                let obj = JSON.parse(xhr.responseText);
                switch(elem){
                    case 'USD':{
                        curItem.innerHTML = `<i class="fas fa-dollar-sign"></i>: rate-${obj[0].rate.toFixed(3)}`;
                        break;
                    }
                    case 'EUR':{
                        curItem.innerHTML = `<i class="fas fa-euro-sign"></i>: rate-${obj[0].rate.toFixed(3)}`;
                        break;
                    }
                    case 'RUB':{
                        curItem.innerHTML = `<i class="fas fa-ruble-sign"></i>: rate-${obj[0].rate.toFixed(3)}`;
                        break;
                    }
                }
                currency.appendChild(curItem);
            }
        });
    }
}

document.getElementById('cur_btn').addEventListener('click', function(){
    (curDate.value === todayDate()) ? getTodayCur() : getPastCur();
});

/*--------XMLHttpRequest-weather--------*/

let temp = document.createElement('p');
let pressure = document.createElement('p');
let humidity = document.createElement('p');
let wind = document.createElement('p');
let clouds = document.createElement('p');

dataInitialization('Kyiv');

function dataInitialization(city){
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=dc2e97b74d8a60f99ea2af5cbb467920`, false);
    xhr.send();

    if(xhr.status != 200){
        console.log(xhr.statusText);
    } else {
        let obj = JSON.parse(xhr.responseText);
        let weather = document.getElementById('weather');

        temp.innerHTML = `Температура: ${parseInt(obj.main.temp - 273.15)} <sup>o</sup>C`;
        weather.appendChild(temp);

        pressure.innerText = `Давление: ${parseInt(obj.main.pressure * 0.75006375541921)} мм рт. ст.`;
        weather.appendChild(pressure);

        humidity.innerText = `Влажность: ${obj.main.humidity} %`;
        weather.appendChild(humidity);

        wind.innerText = `Ветер: ${obj.wind.speed} м/с`;
        weather.appendChild(wind);

        clouds.innerText = `Облачнось: ${obj.clouds.all} %`;
        weather.appendChild(clouds);
    }
}

document.getElementById('weather_btn').addEventListener('click', function(){
    let list = document.getElementById('cities').options;
    for(let i = 0; i < list.length; i++){
        if(list[i].selected){
            dataInitialization(list[i].value);
        }
    }
});